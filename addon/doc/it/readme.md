# Focus Highlight #

* Autore: Takuya Nishimoto
* Scarica la [versione stabile][2]
* Scarica la [versione in sviluppo][1]

Disegnando un rettangolo colorato, questo addon consente agli utenti
ipovedenti, educatori vedenti o agli sviluppatori di tenere traccia della
posizione dell'oggetto su cui si trova il navigatore ad oggetti, oppure
dell'oggetto che ha il focus.

Sono utilizzati i colori seguenti:

* Un rettangolo formato da sottili linee verdi tratteggiate e punteggiate,
  per indicare la posizione del navigatore ad oggetti
* Un Rettangolo sottile rosso, per indicare l'oggetto o il controllo che ha
  il focus
* Un Rettangolo spesso rosso, per indicare quando il navigatore di oggetti e
  l'oggetto mirato si sovrappongono.
* Un rettangolo spesso e blu con linee sottili e punteggiate sta ad indicare
  che NVDA è in modalità focus, ad esempio ciò che scriviamo viene
  visualizzato a schermo.

Per disabilitare il monitor  dell'oggetto, disinstallare l'addon.

## Changes for 5.6 ##

* Nuove Traduzioni e aggiornamenti di quelle esistenti.
* Addresses the compatibility issue with NVDA snapshot alpha-16682.

## Changes for 5.5 ##

* Addresses the issue with NVDA 2018.4 and Firefox/Chrome web browsers.

## Changes for 5.4 ##

* Nuove Traduzioni e aggiornamenti di quelle esistenti.
* Addresses [the issue](https://github.com/nvdajp/focusHighlight/issues/11)
  regarding version compatibility.

## Changes for 5.3 ##

* Nuove Traduzioni e aggiornamenti di quelle esistenti.
* Addresses [the issue](https://github.com/nvdajp/focusHighlight/issues/10)
  regarding Chrome browser and application sleep mode.

## Changes for 5.2 ##

* Nuove Traduzioni e aggiornamenti di quelle esistenti.

## Changes for 5.1 ##

* Removed debug log output.

## Cambiamenti per la 5.0. ##

* Sono stati modificati gli indicatori della modalità focus e del navigatore
  ad oggetti.
* Sono supportati monitor multipli
* Per la visualizzazione ora viene usata la tecnologia GDI Plus.

## Cambiamenti per la 4.0. ##

* Nasconde il rettangolo se l'applicazione corrente è in modalità riposo.

## Cambiamenti per la 3.0. ##

* Risolto un problema con le caselle combinate espanse.
* Risolto un problema con il gestore attività di Windows.
* Capacità di indicare la modalità focus.

## Cambiamenti per la 2.1 ##

* Nuove Traduzioni e aggiornamenti di quelle esistenti.

## Cambiamenti per la 2.0 ##

* L'aiuto è disponibile dalla gestione componenti aggiuntivi di NVDA.

## Cambiamenti per la 1.1 ##

* Modificato il navigatore ad oggetti, da rettangolo a linea frastagliata.
* Risolto un problema con il caricamento dei plugin.

## Cambiamenti per la 1.0 ##

* In Internet Explorer 10 e in Skype su Windows 8, risolto un problema con
  il navigatore ad oggetti.
* Versione iniziale.


[[!tag dev stable]]

[1]: https://addons.nvda-project.org/files/get.php?file=fh-dev

[2]: https://addons.nvda-project.org/files/get.php?file=fh
