# Focus Highlight #

* Autores: Takuya Nishimoto
* Descargar [versión estable][2]
* Descarga [versión de desenvolvemento][1]

Ó dibuxar un rectángulo coloreado, este complemento permite ós usuarios con
deficencia visual, educadores videntes, ou desenvolvedores seguir a posición
do navegador de obxectos do NVDA e o obxecto ou control enfocado.

As seguintes cores utilízanse por este complemento:

* Rectángulo verde de liñas ponteadas para indicar o navegador de obxectos.
* Rectángulo fino bermello, para indicar o obxecto ou control enfocados.
* Rectángulo groso bermello, para indicar cando o navegador de obxectos e o
  obxecto enfocado se superpoñen.
* Rectángulo de liñas ponteadas grosas azúis, para indicar que o NVDA está
  en modo foco, esto é, as teclas pásanse ao control.

Para deshabilitar o seguemento de obxectos, desinstala o complemento.

## Cambios para 5.6 ##

* Traduccións novas e actualizadas.
* Arranxa o problema de compatibilidade coa compilación de desenvolvemento
  de NVDA alpha-16682.

## Cambios para 5.5 ##

* Soluciona o problema con NVDA 2018.4 e os navegadores web Firefox/Chrome.

## Cambios para 5.4 ##

* Traduccións novas e actualizadas.
* Soluciona [o problema](https://github.com/nvdajp/focusHighlight/issues/11)
  referente ás versións compatibles.

## Cambios para 5.3 ##

* Traduccións novas e actualizadas.
* Soluciona [o problema](https://github.com/nvdajp/focusHighlight/issues/10)
  en relación co navegador Chrome e o modo de aplicación durminte.

## Cambios para 5.2 ##

* Traduccións novas e actualizadas.

## Cambios para 5.1 ##

* Eliminada a saída ó rexistro de depuración.

## Cambios para 5.0 ##

* Cambiáronse os indicadores para o navegador de obxectos e para o Modo
  Foco.
* Admítense múltiples monitores.
* Agora usa a tecnoloxía GDI Plus para dibuxar.

## Cambios para 4.0 ##

* Agocha o rectángulo se a aplicación actual está en modo durminte.

## Cambios para 3.0 ##

* Arranxado un problema vencellado coa Caixa combinada expandida.
* Correxido un problema co xestor de tarefas de Windows.
* Capacidade para indicar modo foco.

## Cambios para 2.1 ##

* Traduccións novas e actualizadas.

## Cambios para 2.0 ##

* A axuda do complemento está dispoñible no Administrador de Complementos.

## Cambios para 1.1 ##

* Cambiado o rectángulo do navegador de obxectos por unha liña irregular.
* Correxido un problema con 'Recargar plugins'.

## Cambios para  1.0 ##

* No Internet Explorer 10 e no Skype en Windows 8, arránxase un problema co
  navegador de obxectos.
* Versión inicial.


[[!tag dev stable]]

[1]: https://addons.nvda-project.org/files/get.php?file=fh-dev

[2]: https://addons.nvda-project.org/files/get.php?file=fh
