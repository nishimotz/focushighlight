# Focus Highlight #

* Autores: Takuya Nishimoto
* Descargar [versión estable][2]
* Descarga [versión de desarrollo][1]

Al dibujar un rectángulo coloreado, este complemento capacita a los usuarios
con deficiencia visual, educadores videntes, o desarrolladores para seguir
la posición del navegador de objetos de NVDA y del objeto o control
enfocado.

Los siguientes colores se utilizan con este complemento:

* Rectángulo verde de líneas punteadas para indicar el navegador de objetos.
* Rectángulo rojo delgado, para indicar el objeto o control enfocado.
* Rectángulo rojo grueso, para indicar cuando el navegador de objetos y el
  objeto enfocado se superponen.
* Rectángulo de líneas punteadas gruesas azules, para indicar que NVDA está
  en modo foco, es decir, las teclas se pasan al control.

Para deshabilitar el seguimiento de objetos, desinstala el complemento.

## Cambios para 5.6 ##

* Traducciones nuevas y actualizadas.
* Soluciona problemas de compatibilidad con la snapshot de NVDA alpha-16682.

## Cambios para 5.5 ##

* Resuelve la incidencia con NVDA 2018.4 y los navegadores web Firefox y
  Chrome.

## Cambios para 5.4 ##

* Traducciones nuevas y actualizadas.
* Soluciona [la
  incidencia](https://github.com/nvdajp/focusHighlight/issues/11)
  relacionada con la compatibilidad de versiones.

## Cambios para 5.3 ##

* Traducciones nuevas y actualizadas.
* Resuelve [la
  incidencia](https://github.com/nvdajp/focusHighlight/issues/10)
  relacionada con el navegador Chrome y el modo silencioso en aplicaciones.

## Cambios para 5.2 ##

* Traducciones nuevas y actualizadas.

## Cambios para 5.1 ##

* Se ha eliminado la salida del registro de depuración.

## Cambios para 5.0 ##

* Se cambiaron los indicadores para navegador de objetos y Modo Foco.
* Se admiten múltiples monitores.
* Ahora utiliza la tecnología GDI Plus para dibujar.

## Cambios para 4.0 ##

* Oculta el rectángulo si la aplicación actual está en modo durmiente.

## Cambios para 3.0 ##

* Corregido un problema relacionado con el cuadro combinado expandido.
* Corregido un problema  con el gestor de tareas de Windows.
* Capacidad para indicar el modo foco.

## Cambios para 2.1 ##

* Traducciones nuevas y actualizadas.

## Cambios para 2.0 ##

* La ayuda del complemento está disponible en el Administrador de
  Complementos.

## Cambios para 1.1 ##

* Se cambió el rectángulo del navegador de objetos por  una línea quebrada.
* Corregido un problema  con 'Recargar plugins'.

## Cambios para 1.0 ##

* En Internet Explorer 10 y en Skype en Windows 8,se soluciona un problema
  con el navegador de objetos.
* Versión inicial.


[[!tag dev stable]]

[1]: https://addons.nvda-project.org/files/get.php?file=fh-dev

[2]: https://addons.nvda-project.org/files/get.php?file=fh
