# Focus hervorheben #

* Authoren: Takuya Nishimoto
* [Stabile Version ][2] herunterladen
* [Entwicklungsversion][1] herunterladen

Durch Zeichnen eines farbigen Rechtecks ermöglicht diese Erweiterung
sehbehinderten Nutzern, sehenden Lehrern oder Entwicklern die Position des
fokusierten Objektes sowie des Navigator-Objektes auf dem Bildschirm zu
verfolgen.

Die folgenden Farben werden von dieser Erweiterung verwendet:

* Grüne, düne, gestrichelte und gepunktete Linie, zeigt das Objekt, auf dem
  der Navigator steht.
* Rotes, dünnes Rechteck: Position des fokusierten Objekts anzeigen.
* Ein rotes breites Rechteck zeigt an, dass Fokus und Navigator auf dem
  gleichen Objekt stehen.
* Blaues dickes gepunktetes Rechteck, um zu zeigen, dass NVDA im Fokus-Modus
  ist, d.h. Tastenanschläge werden an das aktuelle Steuerelement übergeben.

Um das hervorheben von Objekten zu deaktivieren, deinstallieren Sie diese
Erweiterung.

## Änderungen für 5.6 ##

* Neue und aktualisierte Übersetzungen.
* Behebt das Kompatibilitätsproblem mit dem NVDA-Snapshot alpha-16682.

## Änderungen für 5.5 ##

* Behebt das Problem mit NVDA 2018.4 und Firefox/Chrome-Internet-Browsern.

## Änderungen für 5.4 ##

* Neue und aktualisierte Übersetzungen.
* Behebt den [Fehler](https://github.com/nvdajp/focusHighlight/issues/11)
  bzgl. der Versionskompatibilität.

## Änderungen für 5.3 ##

* Neue und aktualisierte Übersetzungen.
* Behebt den [Fehler](https://github.com/nvdajp/focusHighlight/issues/10)
  bzgl. Chrome-Browser und Schlafmodus von Anwendungen.

## Änderungen für 5.2 ##

* Neue und aktualisierte Übersetzungen.

## Änderungen für 5.1 ##

* Die Protokollierungsstufe "debug"  wurde entfernt.

## Änderungen in 5.0 ##

* Die Anzeige für den Fokusmodus und für das Navigator-Objekt wurde
  geändert.
* Unterstützt multiple Bildschirme.
* Für Drawing wird nun GDI Plus verwendet.

## Änderungen in 4.0 ##

* Sobald die aktuelle Anwendung im Schlafmodus ist, wird das Rechteck
  ausgeblendet.

## Änderungen in 3.0 ##

* Es wurde ein Problem bei erweiterten Kombinationsfeldern behoben.
* Fehler mit dem Windows-Task-Manager behoben.
* Fähigkeit den Lesemodus anzuzeigen.

## Änderungen in 2.1 ##

* Neue und aktualisierte Übersetzungen.

## Änderungen in 2.0 ##

* Die Hilfe ist nun über den Erweiterungs-Manager verfügbar.

## Änderungen in 1.1 ##

* Das Navigator-Objekt wird nun mit einer gezackten Linie umrandet.
* Fehler behoben, der beim neuladen von Plugins auftrat.

## Änderungen in 1.0 ##

* Problem mit dem Navigator-Objekt in Internet Explorer 10 und Skype für
  Windows 8 behoben.
* Anfängliche Version.


[[!tag dev stable]]

[1]: https://addons.nvda-project.org/files/get.php?file=fh-dev

[2]: https://addons.nvda-project.org/files/get.php?file=fh
