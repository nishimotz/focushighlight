# Focus Highlight #

* Autori: Takuya Nishimoto
* Descarcă [Versiunea Stabilă][2]
* Descarcă [versiunea în dezvoltare][1]

La desenarea unui dreptunghi colorat, acest supliment le permite
utilizatorilor cu vedere parțială, educatorilor văzători, sau
dezvoltatorilor să urmărească locația navigatorului de obiecte NVDA și
obiectul/controlul focalizat.

Culorile utilizate de către acest add-on sunt:

* Linie verde zimțată pentru a indica obiectul navigatorului.
* Dreptunghi roșu subțire, pentru a indica obiectul sau controlul focalizat.
* Dreptunghi roșu gros, pentru a indica când obiectul navigatorului și
  obiectul focalizat se suprapun.
* Dreptunghi gros albastru cu peruri subțiri, pentru a indica faptul că NVDA
  este în modul focalizare, adică tipurile de taste sunt transmise la
  control.

Pentru a dezactiva track obiect, dezinstalează add-on-ul.

## Modificări aduse în versiunea 5.6 ##

* Traduceri noi și actualizate.
* Rezolvă problema de compatibilitate cu snapshot-ul NVDA alpha-16682.

## Modificări aduse în versiunea 5.5 ##

* Adresa problemei în legătură cu NVDA 2018.4 și navigatoarele web Firefox &
  Chrome.

## Modificări aduse în versiunea 5.4 ##

* Traduceri noi și actualizate.
* Adresa [problema](https://github.com/nvdajp/focusHighlight/issues/11) în
  legătură cu compatibilitatea versiunii.

## Modificări aduse în versiunea 5.3 ##

* Traduceri noi și actualizate.
* Adresa [problemei](https://github.com/nvdajp/focusHighlight/issues/10) în
  legătură cu navigatorul Chrome și modul de repaus al aplicației.

## Modificări aduse în versiunea 5.2 ##

* Traduceri noi și actualizate.

## Modificări aduse în versiunea 5.1 ##

* A fost eliminată ieșirea jurnalului diagnosticării.

## Modificări aduse în versiunea 5.0 ##

* Indicatorii obiectului navigator au fost modificați.
* Sunt suportate monitoare multiple.
* Acum, utilizează tehnologia GDI Plus pentru desen.

## Modificări aduse în versiunea 4.0 ##

* Ascunde modul dreptunghi dacă aplicația curentă este în modul de
  hibernare.

## Modificări aduse în versiunea 3.0 ##

* S-a rezolvat problema cu privire la casetele combinate extinse.
* A fost rezolvată problema cu managerul de activități Windows.
* Capabilitatea de a indica modul de focalizare.

## Modificări aduse în versiunea 2.1 ##

* Traduceri noi și actualizate.

## Modificări aduse în 2.0 ##

* Ajutorul suplimentului este valabil din managerul de add-on-uri.

## Modificări aduse în versiunea 1.1 ##

* A fost modificat obiectul navigatorului la linie zâmțată.
* A fost rezolvată problema cu "Reîncarcă plugin-urile".

## Modificări aduse în 1.0 ##

* În Internet Explorer 10 și în Skype pe Windows 8, a fost rezolvată
  problema cu obiectul navigatorului.
* Versiunea inițială.


[[!tag dev stable]]

[1]: https://addons.nvda-project.org/files/get.php?file=fh-dev

[2]: https://addons.nvda-project.org/files/get.php?file=fh
